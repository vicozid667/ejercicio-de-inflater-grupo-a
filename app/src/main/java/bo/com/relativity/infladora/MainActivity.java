package bo.com.relativity.infladora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    LayoutInflater layoutInflater;

    LinearLayout mParent;

    View myView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mParent = findViewById(R.id.mParent);

        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        cargarViewsPersonalizado();

    }

    private void cargarViewsPersonalizado() {
        for (int i = 0; i < 10; i++) {
            myView = layoutInflater.inflate(R.layout.example_design,null,false);
            mParent.addView(myView);
        }


    }
}

